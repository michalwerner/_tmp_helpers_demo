import * as R from "rambdax"
import * as React from "react"
import { Maybe } from "true-myth"

import { Spinner } from "../../components/Spinner"
import { err } from "../logging"
import { Loadable, LoadableArray } from "./Loadable"

interface LoadableRenderCommon {
  pristine?: () => JSX.Element
  error?: (error: string) => JSX.Element
}

interface LoadableRender<T> extends LoadableRenderCommon {
  loading?: (data: Maybe<T>) => JSX.Element
  loaded?: (data: T) => JSX.Element
  endOfData?: (data: T) => JSX.Element
}

interface LoadableArrayRender<T> extends LoadableRenderCommon {
  loading?: (data: T[]) => JSX.Element
  loaded?: (data: T[]) => JSX.Element
  endOfData?: (data: T[]) => JSX.Element
}

export const loadableComponentCommon = <T extends {}>(
  loadable: Loadable<T> | LoadableArray<T>,
  renders: LoadableRender<T> | LoadableArrayRender<T>,
): JSX.Element => {
  if (loadable.status === "pristine") {
    return R.defaultTo(() => <></>, renders.pristine)()
  }
  if (loadable.status === "error") {
    return Maybe.of(renders.error).unwrapOr(error => <div className="error">{error}</div>)(
      loadable.error.unwrapOr("Error message not provided"),
    )
  }
  return err(`Unexpected loadable status ${loadable.status}`)
}
export const loadableComponent = <T extends {}>(
  loadable: Loadable<T>,
  renders: LoadableRender<T>,
): JSX.Element => {
  if (loadable.status === "loading") {
    return R.defaultTo(() => <Spinner />, renders.loading)(loadable.data)
  }
  if (["loaded", "endOfData"].includes(loadable.status)) {
    return loadable.data.match<JSX.Element>({
      Just: data => R.defaultTo((_data: T) => <></>, renders.loaded)(data),
      Nothing: () => err("Loadable is loaded, but there is no data"),
    })
  }
  return loadableComponentCommon(loadable, renders)
}

export const loadableArrayComponent = <T extends {}>(
  loadable: LoadableArray<T>,
  renders: LoadableArrayRender<T>,
): JSX.Element => {
  if (loadable.status === "loading") {
    return R.defaultTo(() => <Spinner />, renders.loading)(loadable.data)
  }
  if (["loaded", "endOfData"].includes(loadable.status)) {
    return R.defaultTo((_data: T[]) => <></>, renders.loaded)(loadable.data)
  }
  return loadableComponentCommon(loadable, renders)
}
