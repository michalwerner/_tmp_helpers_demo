import * as R from "rambdax"
import { combineReducers } from "redux"
import { Maybe } from "true-myth"

import { AppAction, PayloadAction } from "../../store/actionsUtils"
import { AppReducer } from "../../store/reducersUtils"
import { invariant } from "../logging"
import { Loadable, LoadableArray, LoadableStatus } from "./Loadable"

interface LoadableTriggers {
  pristine?: string[]
  loading?: string[]
  loaded?: string[]
  error?: string[]
  endOfData?: string[]
}

const mkIsTriggered = (triggers: LoadableTriggers, action: AppAction) => (
  statuses: LoadableStatus[],
): boolean =>
  statuses.some(status =>
    Maybe.of(triggers[status])
      .map(R.includes(action.type))
      .unwrapOr(false),
  )

const mkStatusReducer = (triggers: LoadableTriggers) => (
  state: LoadableStatus = "pristine",
  action: AppAction,
): LoadableStatus => {
  const isTriggered = mkIsTriggered(triggers, action)
  if (isTriggered(["pristine"])) {
    return "pristine"
  }
  if (isTriggered(["loading"])) {
    return "loading"
  }
  if (isTriggered(["loaded"])) {
    return "loaded"
  }
  if (isTriggered(["endOfData"])) {
    return "endOfData"
  }
  if (isTriggered(["error"])) {
    return "error"
  }
  return state
}

const mkErrorReducer = (triggers: LoadableTriggers) => (
  state: Maybe<string> = Maybe.nothing(),
  action: AppAction,
): Maybe<string> => {
  const isTriggered = mkIsTriggered(triggers, action)
  if (isTriggered(["error"])) {
    return Maybe.of((action as PayloadAction<string>).payload)
  }
  if (isTriggered(["pristine", "loading", "loaded", "endOfData"])) {
    return Maybe.nothing()
  }
  return state
}

const checkTriggers = (triggers: LoadableTriggers): void => {
  const allTriggers = [
    ...(triggers.pristine || []),
    ...(triggers.loading || []),
    ...(triggers.loaded || []),
    ...(triggers.endOfData || []),
    ...(triggers.error || []),
  ]
  invariant(R.uniq(allTriggers).length === allTriggers.length, "Loadable triggers are not unique")
}

export const mkLoadableReducer = <T>(
  dataReducer: AppReducer<Maybe<T>>,
  triggers: LoadableTriggers,
): AppReducer<Loadable<T>> => {
  checkTriggers(triggers)
  return combineReducers({
    status: mkStatusReducer(triggers),
    data: dataReducer,
    error: mkErrorReducer(triggers),
  })
}

export const mkLoadableArrayReducer = <T>(
  dataReducer: AppReducer<T[]>,
  triggers: LoadableTriggers,
): AppReducer<LoadableArray<T>> => {
  checkTriggers(triggers)
  return combineReducers({
    status: mkStatusReducer(triggers),
    data: dataReducer,
    error: mkErrorReducer(triggers),
  })
}
