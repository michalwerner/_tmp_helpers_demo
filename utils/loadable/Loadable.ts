import { Maybe } from "true-myth"

export type LoadableStatus = "pristine" | "loading" | "loaded" | "endOfData" | "error"
export const loadableStatusValues: LoadableStatus[] = [
  "pristine",
  "loading",
  "loaded",
  "endOfData",
  "error",
]

interface LoadableCommon {
  status: LoadableStatus
  error: Maybe<string>
}

export interface Loadable<T> extends LoadableCommon {
  data: Maybe<T>
}

export interface LoadableArray<T> extends LoadableCommon {
  data: T[]
}
