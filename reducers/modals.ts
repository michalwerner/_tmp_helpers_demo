import { combineReducers } from "redux"

import { CLOSE_MODAL, OPEN_MODAL } from "../actions/modals"
import { ModalName, modalNameValues, ModalsRecord } from "../models/Modal"
import { ModalsState } from "../models/state"
import { PayloadAction } from "../store/actionsUtils"
import { mkReducer } from "../store/reducersUtils"

const record = mkReducer<ModalsRecord>(
  [
    {
      actionTypes: [OPEN_MODAL, CLOSE_MODAL],
      fn: (state, action: PayloadAction<ModalName>) => ({
        ...state,
        [action.payload]: {
          ...state[action.payload],
          isOpen: action.type === OPEN_MODAL,
        },
      }),
    },
  ],
  modalNameValues.reduce(
    (acc, val) => ({
      ...acc,
      [val]: { isOpen: false },
    }),
    /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
    {} as any,
  ),
)

export const modals = combineReducers<ModalsState>({ record })
