import * as R from "rambdax"

import { CLEAR_ALL_TRANSACTIONS, FETCH_ALL_TRANSACTIONS, SET_ALL_TRANSACTIONS } from "../actions/transactions"
import { BackendTransaction, Transaction, transactionFromBackend } from "../models/Transaction"
import { mkArrayValueReducer } from "../store/reducersUtils"
import { mkLoadableArrayReducer } from "../utils/loadable/loadableReducer"

const transactionsData = mkArrayValueReducer<Transaction, BackendTransaction>(
  {
    set: [SET_ALL_TRANSACTIONS],
    clear: [FETCH_ALL_TRANSACTIONS, CLEAR_ALL_TRANSACTIONS],
  },
  R.map<BackendTransaction, Transaction>(transactionFromBackend),
)

export const transactions = mkLoadableArrayReducer(transactionsData, {
  pristine: [CLEAR_ALL_TRANSACTIONS],
  loading: [FETCH_ALL_TRANSACTIONS],
  loaded: [SET_ALL_TRANSACTIONS],
})
