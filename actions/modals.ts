import { ModalName } from "../models/Modal"
import { mkPayloadActionCreator } from "../store/actionsUtils"

const prefix = "@MODALS"

export const OPEN_MODAL = `${prefix}/OPEN`
export const openModal = mkPayloadActionCreator<ModalName>(OPEN_MODAL)

export const CLOSE_MODAL = `${prefix}/CLOSE`
export const closeModal = mkPayloadActionCreator<ModalName>(CLOSE_MODAL)
