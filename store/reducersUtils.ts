import { Maybe } from "true-myth"

import { AppAction, PayloadAction } from "./actionsUtils"

export type AppReducer<T, A extends AppAction = AppAction> = (state: T, action: A) => T

interface Case<T, A extends AppAction> {
  actionTypes: string[]
  fn: (state: T, action: A) => NonNullable<T>
}

export const mkReducer = <T, A extends AppAction = AppAction>(
  cases: Case<T, A>[],
  initial: NonNullable<T>,
) => (state: NonNullable<T> = initial, action: A): NonNullable<T> =>
  Maybe.of(cases.find(c => c.actionTypes.includes(action.type)))
    .map(c => c.fn(state, action))
    .unwrapOr(state)

interface BoolReducerTriggers {
  true?: string[]
  false?: string[]
  toggle?: string[]
}

export const mkBoolReducer = (
  triggers: BoolReducerTriggers,
  initial: boolean,
): AppReducer<boolean> => {
  const trueCases: Case<boolean, AppAction>[] = Maybe.of(triggers.true)
    .map(actionTypes => [{ actionTypes, fn: () => true }])
    .unwrapOr([])
  const falseCases: Case<boolean, AppAction>[] = Maybe.of(triggers.false)
    .map(actionTypes => [{ actionTypes, fn: () => false }])
    .unwrapOr([])
  const toggleCases: Case<boolean, AppAction>[] = Maybe.of(triggers.toggle)
    .map(actionTypes => [{ actionTypes, fn: (state: boolean) => !state }])
    .unwrapOr([])
  return mkReducer([...trueCases, ...falseCases, ...toggleCases], initial)
}

interface ValueReducerTriggers {
  set?: string[]
  clear?: string[]
}

export const mkValueReducer = <T, P = T>(
  triggers: ValueReducerTriggers,
  setFn: (payload: P) => T,
  initial: Maybe<T> = Maybe.nothing<T>(),
): AppReducer<Maybe<T>, AppAction | PayloadAction<P>> => {
  const setCases: Case<Maybe<T>, PayloadAction<P>>[] = Maybe.of(triggers.set)
    .map(actionTypes => [
      {
        actionTypes,
        fn: (_state: Maybe<T>, action: PayloadAction<P>) => Maybe.of(setFn(action.payload)),
      },
    ])
    .unwrapOr([])
  const clearCases: Case<Maybe<T>, PayloadAction<P>>[] = Maybe.of(triggers.clear)
    .map(actionTypes => [
      {
        actionTypes,
        fn: () => Maybe.nothing<T>(),
      },
    ])
    .unwrapOr([])
  return mkReducer([...setCases, ...clearCases], initial)
}

interface ArrayValueReducerTriggers {
  set?: string[]
  append?: string[]
  clear?: string[]
}

export const mkArrayValueReducer = <T, P = T>(
  triggers: ArrayValueReducerTriggers,
  setFn: (payload: P[]) => T[],
  initial: T[] = [],
): AppReducer<T[], AppAction | PayloadAction<P[]>> => {
  const setCases: Case<T[], PayloadAction<P[]>>[] = Maybe.of(triggers.set)
    .map(actionTypes => [
      {
        actionTypes,
        fn: (_state: T[], action: PayloadAction<P[]>) => setFn(action.payload),
      },
    ])
    .unwrapOr([])
  const appendCases: Case<T[], PayloadAction<P[]>>[] = Maybe.of(triggers.append)
    .map(actionTypes => [
      {
        actionTypes,
        fn: (state: T[], action: PayloadAction<P[]>) => [...state, ...setFn(action.payload)],
      },
    ])
    .unwrapOr([])
  const clearCases: Case<T[], PayloadAction<P[]>>[] = Maybe.of(triggers.clear)
    .map(actionTypes => [
      {
        actionTypes,
        fn: () => [],
      },
    ])
    .unwrapOr([])
  return mkReducer([...setCases, ...appendCases, ...clearCases], initial)
}

/* eslint-disable-next-line no-underscore-dangle */
export const __mkUnsafeValueReducer = <T, P = T>(
  triggers: ValueReducerTriggers,
  setFn: (payload: P) => T,
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  initial: T = null as any,
): AppReducer<T, AppAction | PayloadAction<P>> => {
  const setCases: Case<T, PayloadAction<P>>[] = Maybe.of(triggers.set)
    .map(actionTypes => [
      {
        actionTypes,
        /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
        fn: (_state: T, action: PayloadAction<P>) => setFn(action.payload) as any,
      },
    ])
    .unwrapOr([])
  const clearCases: Case<T, PayloadAction<P>>[] = Maybe.of(triggers.clear)
    .map(actionTypes => [
      {
        actionTypes,
        /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
        fn: () => null as any,
      },
    ])
    .unwrapOr([])
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  return mkReducer([...setCases, ...clearCases], initial as any) as any
}
