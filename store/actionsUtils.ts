import { FormikHelpers } from "formik"

export interface AppAction {
  type: string
}

export interface PayloadAction<P> extends AppAction {
  payload: P
}

export type ActionCreator = () => AppAction
export const mkActionCreator = (type: string) => (): AppAction => ({ type })

export type PayloadActionCreator<P> = (payload: P) => PayloadAction<P>
export const mkPayloadActionCreator = <P>(type: string) => (payload: P): PayloadAction<P> => ({
  type,
  payload,
})

export interface FormSubmissionPayload<S> {
  values: S
  helpers: FormikHelpers<S>
}

export interface FormSubmissionAction<S> extends AppAction {
  payload: FormSubmissionPayload<S>
}

export const mkFormSubmissionActionCreator = <S>(type: string) => (
  values: S,
  helpers: FormikHelpers<S>,
): FormSubmissionAction<S> => ({ type, payload: { values, helpers } })

export interface FormSubmissionPayloadWithExtra<S, E> extends FormSubmissionPayload<S> {
  extraPayload: E
}

export interface FormSubmissionExtraPayloadAction<S, E> extends AppAction {
  payload: FormSubmissionPayloadWithExtra<S, E>
}

export const mkFormSubmissionExtraPayloadActionCreator = <S, E>(type: string) => (
  values: S,
  helpers: FormikHelpers<S>,
  extraPayload: E,
): FormSubmissionExtraPayloadAction<S, E> => ({
  type,
  payload: { values, helpers, extraPayload },
})

export const NOOP_ACTION = "NOOP"
export const noopAction = mkActionCreator(NOOP_ACTION)
